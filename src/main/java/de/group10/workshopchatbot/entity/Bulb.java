package de.group10.workshopchatbot.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
created by LStiller
06.12.21 13:20
workshop-chatbot
*/

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Bulb {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  private String name;

  private String brand;

  private double price;

  // voltage = spannung
  private Integer voltage;

  // warm or cold light
  private Integer colourTemp;

  // brightness
  private Integer luminousFlux;

  // capType = fassung
  private String capType;

  // Bulb type
  private String type;

  // wattage = watt
  private Integer wattage;

  // EU Energy Efficiency Label
  private String energyEfficiency;

  private String link;

  public Bulb(String name, double price, Integer luminousFlux, String typeOfBulb,
      String energyEfficiency, Integer voltage, Integer wattage, String brand, String link) {
    this.name = name;
    this.price = price;
    this.luminousFlux = luminousFlux;
    this.type = typeOfBulb;
    this.energyEfficiency = energyEfficiency;
    this.voltage = voltage;
    this.wattage = wattage;
    this.brand = brand;
    this.link = link;
  }
}
