package de.group10.workshopchatbot.entity;

/*
created by LStiller
07.12.21 15:42
workshop-chatbot
*/

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BulbDtoPrice {

    private double price;

    public double getPrice() {
        return price;
    }
}
