package de.group10.workshopchatbot.entity;

/*
created by LStiller
07.12.21 15:43
workshop-chatbot
*/

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BulbDtoBrightness {



    // brightness
    private Integer luminousFlux;
}
