package de.group10.workshopchatbot.controller;

/*
created by LStiller
06.12.21 13:22
workshop-chatbot
*/

import de.group10.workshopchatbot.service.KeywordService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/chatbot")
public class ChatbotController {

  @Autowired
  KeywordService keywordService;

  @GetMapping("/")
  public ResponseEntity<List> startConvo(
      @RequestParam(value = "keyword", required = false) String keyword,
      @RequestParam(value = "type", required = false) String type,
      @RequestParam(value = "numberToSort", required = false) Integer numberToSort) {
    return new ResponseEntity<>(keywordService.bulbSpecificConversation(type, keyword, numberToSort),
        HttpStatus.OK);
  }

  @GetMapping("/id")
  public ResponseEntity<String> getId(
      @RequestParam(value = "id", required = false) Integer id) {
    return new ResponseEntity<>(keywordService.getBulbById(id),
        HttpStatus.OK);
  }
}
