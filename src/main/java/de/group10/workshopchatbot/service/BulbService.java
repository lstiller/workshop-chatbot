package de.group10.workshopchatbot.service;

/*
created by LStiller
06.12.21 13:24
workshop-chatbot
*/

import de.group10.workshopchatbot.entity.Bulb;
import de.group10.workshopchatbot.repository.BulbRepository;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BulbService {

  @Autowired
  BulbRepository bulbRepository;

  public Optional<Bulb> getBulbById(Integer id) {
    return bulbRepository.findById(id);
  }

  public List<Bulb> getAllBulbs() {
    return bulbRepository.findAll();
  }

  public List<Bulb> getBulbsType(String type) {
    return bulbRepository.findByType(type.toUpperCase(Locale.ROOT));
  }

  public List<Bulb> getByPriceGreaterThan10(String type) {

    ArrayList<Bulb> bulbList = (ArrayList<Bulb>) bulbRepository.findByType(
        type.toUpperCase(Locale.ROOT));

    for (int i = 0; i < bulbList.size(); i++) {
      Bulb bulb = bulbList.get(i);
      if (bulb.getPrice() < 10) {
        bulbList.remove(i);
      }

    }

    return bulbList;
  }

  public List<Bulb> getByPriceLessThan10(String type) {

    ArrayList<Bulb> bulbList = (ArrayList<Bulb>) bulbRepository.findByType(
        type.toUpperCase(Locale.ROOT));

    if (!bulbList.isEmpty()) {

      try {

        for (int i = 0; i < bulbList.size(); i++) {
          Bulb bulb = bulbList.get(i);
          if (bulb.getPrice() > 10) {
            bulbList.remove(i);
          }
        }
        for (int i = 0; i < bulbList.size(); i++) {
          Bulb bulb = bulbList.get(i);
          if (bulb.getPrice() > 10) {
            bulbList.remove(i);
          }
        }

        for (int i = 0; i < bulbList.size(); i++) {
          Bulb bulb = bulbList.get(i);
          if (bulb.getPrice() > 10) {
            bulbList.remove(i);
          }
        }
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    }

    return bulbList;
  }

//  public List<Bulb> getByWattageGreaterThanOrEqualTo15(String type) {
//
//    ArrayList<Bulb> bulbList = (ArrayList<Bulb>) bulbRepository.findByType(
//        type.toUpperCase(Locale.ROOT));
//
//    for (int i = 0; i < bulbList.size(); i++) {
//      Bulb bulb = bulbList.get(i);
//      if (bulb.getWattage() < 15) {
//        bulbList.remove(i);
//      }
//    }
//
//    return bulbList;
//  }

//  public List<Bulb> getByWattageLessThan15(String type) {
//
//    ArrayList<Bulb> bulbList = (ArrayList<Bulb>) bulbRepository.findByType(
//        type.toUpperCase(Locale.ROOT));
//
//    if (!bulbList.isEmpty()) {
//
//      try {
//
//        for (int i = 0; i < bulbList.size(); i++) {
//          Bulb bulb = bulbList.get(i);
//          if (bulb.getWattage() > 15) {
//            bulbList.remove(i);
//          }
//        }
//        for (int i = 0; i < bulbList.size(); i++) {
//          Bulb bulb = bulbList.get(i);
//          if (bulb.getWattage() > 15) {
//            bulbList.remove(i);
//          }
//        }
//
//        for (int i = 0; i < bulbList.size(); i++) {
//          Bulb bulb = bulbList.get(i);
//          if (bulb.getWattage() > 15) {
//            bulbList.remove(i);
//          }
//        }
//      } catch (Exception e) {
//        System.out.println(e.getMessage());
//      }
//    }
//
//    return bulbList;
//  }
}
