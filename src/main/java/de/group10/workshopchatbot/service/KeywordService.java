package de.group10.workshopchatbot.service;

/*
created by LStiller
06.12.21 13:42
workshop-chatbot
*/

import de.group10.workshopchatbot.entity.BulbDtoBrightness;
import de.group10.workshopchatbot.entity.BulbDtoEfficiency;
import de.group10.workshopchatbot.entity.BulbDtoPrice;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KeywordService {

  @Autowired
  BulbService bulbService;

  List bulbList = new ArrayList<>();

  public List bulbSpecificConversation(String type, String keyword, Integer numberToSort) {


    if (numberToSort == null) {
      if (type.toLowerCase(Locale.ROOT).contains("led")) {
        if (keyword.toLowerCase(Locale.ROOT).contains("prices")) {
          bulbList.clear();
          for (int i = 0; i < bulbService.getBulbsType("led").size(); i++) {
            BulbDtoPrice bulbDto = new BulbDtoPrice();
            bulbDto.setPrice(bulbService.getBulbsType("led").get(i).getPrice());
            bulbList.add(bulbDto);
            System.out.println("HALLO");
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("voltage")) {
          bulbList.clear();
          for (int i = 0; i <= bulbService.getBulbsType("led").size(); i++) {
            BulbDtoPrice bulbDto = new BulbDtoPrice();
            bulbDto.setPrice(bulbService.getBulbsType("led").get(i).getPrice());
            bulbList.add(bulbDto);
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("wattage")) {
          bulbList.clear();
          for (int i = 0; i < bulbService.getBulbsType("led").size(); i++) {
            BulbDtoPrice bulbDto = new BulbDtoPrice();
            bulbDto.setPrice(bulbService.getBulbsType("led").get(i).getPrice());
            bulbList.add(bulbDto);
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("brand")) {
          bulbList.clear();
          for (int i = 0; i < bulbService.getBulbsType("led").size(); i++) {
            BulbDtoPrice bulbDto = new BulbDtoPrice();
            bulbDto.setPrice(bulbService.getBulbsType("led").get(i).getPrice());
            bulbList.add(bulbDto);
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("efficiency")) {
          bulbList.clear();
          for (int i = 0; i < bulbService.getBulbsType("led").size(); i++) {
            BulbDtoEfficiency bulbDto = new BulbDtoEfficiency();
            bulbDto.setEnergyEfficiency(bulbService.getBulbsType("led").get(i).getEnergyEfficiency());
            bulbList.add(bulbDto);
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("brightness")) {
          bulbList.clear();
          for (int i = 0; i < bulbService.getBulbsType("led").size(); i++) {
            BulbDtoBrightness bulbDto = new BulbDtoBrightness();
            bulbDto.setLuminousFlux(bulbService.getBulbsType("led").get(i).getLuminousFlux());
            bulbList.add(bulbDto);
          }
          return bulbList;
        }
      }

      if (type.toLowerCase(Locale.ROOT).contains("cfl")) {
      }
      if (keyword.toLowerCase(Locale.ROOT).contains("prices")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoPrice bulbDto = new BulbDtoPrice();
          bulbDto.setPrice(bulbService.getBulbsType("cfl").get(i).getPrice());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("voltage")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoPrice bulbDto = new BulbDtoPrice();
          bulbDto.setPrice(bulbService.getBulbsType("cfl").get(i).getPrice());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("wattage")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoPrice bulbDto = new BulbDtoPrice();
          bulbDto.setPrice(bulbService.getBulbsType("cfl").get(i).getPrice());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("brand")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoPrice bulbDto = new BulbDtoPrice();
          bulbDto.setPrice(bulbService.getBulbsType("cfl").get(i).getPrice());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("efficiency")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoEfficiency bulbDto = new BulbDtoEfficiency();
          bulbDto.setEnergyEfficiency(bulbService.getBulbsType("cfl").get(i).getEnergyEfficiency());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("brightness")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoBrightness bulbDto = new BulbDtoBrightness();
          bulbDto.setLuminousFlux(bulbService.getBulbsType("cfl").get(i).getLuminousFlux());
          bulbList.add(bulbDto);
        }
        return bulbList;
      }
    } else {

      if (type.toLowerCase(Locale.ROOT).contains("led")) {
        if (keyword.toLowerCase(Locale.ROOT).contains("prices")) {
          bulbList.clear();
         if (numberToSort > 10) {
            bulbList = bulbService.getByPriceGreaterThan10("led");
          } else if (numberToSort < 10) {
            bulbList = bulbService.getByPriceLessThan10("led");
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("voltage")) {
          bulbList.clear();
          for (int i = 0; i <= bulbService.getBulbsType("led").size(); i++) {
            BulbDtoPrice bulbDto = new BulbDtoPrice();
            bulbDto.setPrice(bulbService.getBulbsType("led").get(i).getPrice());
            bulbList.add(bulbDto);
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("wattage")) {
          bulbList.clear();
//          if (numberToSort >= 15) {
//            bulbList = bulbService.getByWattageGreaterThanOrEqualTo15("led");
//          } else if (numberToSort < 14) {
//            bulbList = bulbService.getByWattageLessThan15("led");
//          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("brand")) {
          bulbList.clear();
          for (int i = 0; i < bulbService.getBulbsType("led").size(); i++) {
            BulbDtoPrice bulbDto = new BulbDtoPrice();
            bulbDto.setPrice(bulbService.getBulbsType("led").get(i).getPrice());
            bulbList.add(bulbDto);
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("efficiency")) {
          bulbList.clear();
          for (int i = 0; i < bulbService.getBulbsType("led").size(); i++) {
            BulbDtoEfficiency bulbDto = new BulbDtoEfficiency();
            bulbDto.setEnergyEfficiency(bulbService.getBulbsType("led").get(i).getEnergyEfficiency());
            bulbList.add(bulbDto);
          }
          return bulbList;
        } else if (keyword.toLowerCase(Locale.ROOT).contains("brightness")) {
          bulbList.clear();
          for (int i = 0; i < bulbService.getBulbsType("led").size(); i++) {
            BulbDtoBrightness bulbDto = new BulbDtoBrightness();
            bulbDto.setLuminousFlux(bulbService.getBulbsType("led").get(i).getLuminousFlux());
            bulbList.add(bulbDto);
          }
          return bulbList;
        }
      }

      if (type.toLowerCase(Locale.ROOT).contains("cfl")) {
      }
      if (keyword.toLowerCase(Locale.ROOT).contains("prices")) {
        bulbList.clear();
        if (numberToSort > 10) {
          bulbList = bulbService.getByPriceGreaterThan10("cfl");
        } else if (numberToSort < 10) {
          bulbList = bulbService.getByPriceLessThan10("cfl");
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("voltage")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoPrice bulbDto = new BulbDtoPrice();
          bulbDto.setPrice(bulbService.getBulbsType("cfl").get(i).getPrice());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("wattage")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoPrice bulbDto = new BulbDtoPrice();
          bulbDto.setPrice(bulbService.getBulbsType("cfl").get(i).getPrice());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("brand")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoPrice bulbDto = new BulbDtoPrice();
          bulbDto.setPrice(bulbService.getBulbsType("cfl").get(i).getPrice());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("efficiency")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoEfficiency bulbDto = new BulbDtoEfficiency();
          bulbDto.setEnergyEfficiency(bulbService.getBulbsType("cfl").get(i).getEnergyEfficiency());
          bulbList.add(bulbDto);
        }
        return bulbList;
      } else if (keyword.toLowerCase(Locale.ROOT).contains("brightness")) {
        bulbList.clear();
        for (int i = 0; i < bulbService.getBulbsType("cfl").size(); i++) {
          BulbDtoBrightness bulbDto = new BulbDtoBrightness();
          bulbDto.setLuminousFlux(bulbService.getBulbsType("cfl").get(i).getLuminousFlux());
          bulbList.add(bulbDto);
        }
        return bulbList;
      }
    }
    return null;
  }

  public String getBulbById(Integer id) {
    return bulbService.getBulbById(id).get().getLink();
  }

}
