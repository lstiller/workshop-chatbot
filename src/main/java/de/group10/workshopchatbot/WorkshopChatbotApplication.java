package de.group10.workshopchatbot;

import de.group10.workshopchatbot.entity.Bulb;
import de.group10.workshopchatbot.repository.BulbRepository;
import java.util.Arrays;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


@SpringBootApplication
public class WorkshopChatbotApplication implements CommandLineRunner {

  @Autowired
  BulbRepository bulbRepository;

  public static void main(String[] args) {
    SpringApplication.run(WorkshopChatbotApplication.class, args);
  }

  @Bean
  public CorsFilter corsFilter() {
    var corsConfiguration = new CorsConfiguration();
    corsConfiguration.setAllowCredentials(true);
    corsConfiguration.setAllowedOrigins(Collections.singletonList("http://localhost:8081s"));
    corsConfiguration.setAllowedHeaders(
        Arrays.asList("Origin", "ACLO", "Content-Type", "Accept", "Authorization", "Origin, Accept",
            "X-Requested-With",
            "Access-Control-Request-Method", "Access-Control-Request-Headers"));
    corsConfiguration.setExposedHeaders(
        Arrays.asList("Origin", "Content-Type", "Accept", "Authorization", "ACLO",
            "Access-Control-Allow-Credentials"));
    corsConfiguration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
    var source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", corsConfiguration);
    return new CorsFilter(source);
  }

  @Override
  public void run(String... args) {

    Bulb bulb = new Bulb("Amazon Basics LED", 7.99, 850, "LED", "5 Stars", 120, 74, "Amazon Basics",
        "https://tinyurl.com/2x3ftnvx");
    bulbRepository.save(bulb);
    Bulb bulb2 = new Bulb("SYLVANIA ECO LED", 15.24, 750, "LED", "4.5 Stars", 120, 8, "LEDVANCE",
        "https://tinyurl.com/2p8skb7t");
    bulbRepository.save(bulb2);
    Bulb bulb3 = new Bulb("SYLVANIA LED", 24.39, 1500, "LED", "4.9 Stars", 120, 12, "LEDVANCE",
        "https://tinyurl.com/3ysec2tz");
    bulbRepository.save(bulb3);
    Bulb bulb4 = new Bulb("SYLVANIA LED A19", 9.79, 500, "LED", "5 Stars", 120, 9, "LEDVANCE",
        "https://tinyurl.com/4p86h35x");
    bulbRepository.save(bulb4);
    Bulb bulb5 = new Bulb("Amazon Basics CFL", 19.99, 1600, "CFL", "4.6 Stars", 120, 15,
        "Amazon Basics", "https://tinyurl.com/2p922urp");
    bulbRepository.save(bulb5);
    Bulb bulb6 = new Bulb("SleekLightning E26 Standard", 19.89, 1600, "CFL", "4.8 Stars", 120, 23,
        "Sleeklightning", "https://tinyurl.com/y92tazva");
    bulbRepository.save(bulb6);
    Bulb bulb7 = new Bulb("SleekLightning GU24", 19.89, 900, "CFL", "5 Stars", 120, 13,
        "Sleeklightning", "https://tinyurl.com/hvm9zwxk");
    bulbRepository.save(bulb7);
    Bulb bulb8 = new Bulb("SYLVANIA CFL", 14.39, 800, "CFL", "4.6 Stars", 120, 13, "LEDVANCE",
        "https://tinyurl.com/mrxk3snc");
    bulbRepository.save(bulb8);
  }
}
