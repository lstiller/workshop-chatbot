package de.group10.workshopchatbot.repository;

/*
created by LStiller
06.12.21 15:45
workshop-chatbot
*/

import de.group10.workshopchatbot.entity.Bulb;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BulbRepository extends CrudRepository<Bulb, Integer> {
    List<Bulb> findAll();
    List<Bulb> findByType(String type);
    Optional<Bulb> findById(Integer id);
}
